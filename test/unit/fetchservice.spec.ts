/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2017                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

// !1!
import {FetchService} from '../../src/services/fetchservice'

describe("FetchService",()=>{
  let fetcher
  beforeEach(()=>{
    fetcher=new FetchService()
  })
  it("fetches JSON objects from remote servers",(done)=>{
    fetcher.fetchJson("fake://energy").then(result=>{
      expect(result).toBeDefined()
      expect(result['val']).toBeDefined()
      done()
    })
  })
  it("gives temperature between -1 and 36",(done)=>{
    fetcher.fetchJsonVal("fake://temperatur").then(result=>{
      expect(result).toBeGreaterThan(-1)
      expect(result).toBeLessThan(36)
      done()
    })
  })
  it("gives humidity between 20 and 80 %",(done)=>{
    fetcher.fetchJsonVal("fake://humidity").then(result=>{
      expect(result).toBeGreaterThan(20)
      expect(result).toBeLessThan(80)
      done()
    })
  })
})
// /!1!
