/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2017                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

// !1!
describe('funktionstest',()=>{
  xit('checks expression',()=>{
      expect(2).toBe(3)
  })
})
// /!1!

// !2!
let raw = await this.http.fetch(url)
return await raw.json()
// /!2!

// !3!

let promiseRaw=this.http.fetch(url)
promiseRaw.then(raw =>{
  let promiseJson=raw.json()
  promiseJson.then(json=>{
    return json
  })
})
return promiseRaw
// /!3!

// !4!

let callback=function(err, result) {
  if (err) {
    alert("Ein Fehler ist passiert:" + err)
  } else {
    alert("Das Resultat ist da!: " + result)
  }
}

function langDauerndeFunktion(done){
  // ich tue hier irgend etwas länger dauerndes
  done(null,"geschafft!")
}

function ichBinDasHauptprogramm() {
  // tu irgendwas
  langDauerndeFunktion(callback)
  // tu was anderes, wir brauchen nicht zu warten
}

// /!4!


// !5!
let result = await fetchService.fetchJsonVal("http://homeview.private/some/value")
// /!5!

// !6!
constructor(private hlp:Helper){}
// /!6!

// !7!
private hlp;
constructor(helper:Helper){
  this.hlp=helper;
}
// /!7!

