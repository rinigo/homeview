/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { FetchService } from './services/fetchservice'
import { autoinject } from 'aurelia-framework'
import { EventAggregator } from 'aurelia-event-aggregator'
import env from './environment'
const dev = env.devices
import configs from './config'
import { tristateMessage } from './components/tristatebutton';
import { Boiler } from "./routes/boiler";
@autoinject
export class App {
  fetcher = new FetchService()
  conf = configs
  private gauges = [configs.wohnzimmertemp_cfg, configs.aussentemp_cfg, configs.bad_oben_cfg,
  configs.dusche_cfg, configs.dachstock_cfg, configs.barometer_cfg, configs.powermeter_cfg]
  private switches = [configs.fernsehlicht_cfg, configs.carloader_cfg, configs.mediacenter_cfg, configs.extender_cfg, configs.e14_cfg, configs.aussenlicht_cfg]
  // private charts = [configs.aussen_chart_cfg, configs.barometer_chart_cfg]

  constructor(private ea: EventAggregator, boiler: Boiler) { }

  private loop = () => {
    this.getGaugeValues(this.gauges)
    this.getSwitchValues(this.switches)
    // this.updateCharts(this.charts)
    setTimeout(this.loop, 5000)

  }

  attached() {
    this.subscribe(this.switches)
    setTimeout(this.loop, 200)

  }

  /**
   * Subscribe to messages of all configured TriStateButtons and PushButtons
   * and send changed values to ioBroker
   * @param switches Array with Button configurationa
   */
  subscribe(switches) {
    switches.forEach(sw => {
      this.ea.subscribe(sw.message, msg => {
        if (msg.direction === "out") {
          let value = configs.SWITCH_AUTO;
          if (msg.mode != undefined) {
            if (msg.mode === "manual") {
              value = (msg.state == "on") ? configs.SWITCH_ON : configs.SWITCH_OFF
            }
            this.fetcher.setIoBrokerValue(sw.mode_id, value)
          } else {
            this.fetcher.setIoBrokerValue(sw.state_id, msg.state === "on" ? true : false)
          }
        }
      })
    });
  }

  /**
   * Fetch ioBroker values for all configured Gauges
   * @param gauges Array with gauge configurations
   */
  getGaugeValues(gauges) {
    let gauge_ids = []
    let gauge_messages = []
    gauges.forEach(dev => {
      gauge_ids = gauge_ids.concat(dev.devices)
      gauge_messages = gauge_messages.concat(dev.message)

    })
    this.fetcher.getIobrokerValues(gauge_ids).then(results => {
      for (let i = 0; i < results.length; i++) {
        this.ea.publish(gauge_messages[i], parseFloat(results[i]))
      }
    }, reason => {
      console.log("an error occured " + reason)
      for (let i = 0; i < gauge_ids.length; i++) {
        this.ea.publish(gauge_messages[i], "?")
      }
      // -> inactivate gauge
    }).catch(err => {
      console.log(err);
    })
  }

  /**
   * Fetch ioBroker values for all configured TristateButtons
   * @param switches Array with tristatebutton configurations
   */
  getSwitchValues(switches) {
    switches.forEach(async sw => {
      let result: tristateMessage = <tristateMessage>{ direction: "in" }
      if (sw.power_id) {
        result.power = await this.fetcher.getIobrokerValue(sw.power_id);
      }
      if (sw.mode_id) {
        let mode = await this.fetcher.getIobrokerValue(sw.mode_id);
        if (mode === configs.SWITCH_AUTO) {
          result.mode = "auto"
        } else {
          result.mode = "manual"
        }
      }
      if (sw.state_id) {
        result.state = (await this.fetcher.getIobrokerValue(sw.state_id)) ? "on" : "off"
      }
      this.ea.publish(sw.message, result)
    })
  }

  updateCharts(charts) {
    charts.forEach(chart => {
      this.ea.publish(chart.message)
    })
  }

  switchGauge(hide, show) {
    this.conf[hide].visible = false
    this.conf[show].visible = true
  }
}
