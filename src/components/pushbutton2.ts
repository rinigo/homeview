import { autoinject, noView, bindable } from "aurelia-framework";
import { Component, eaMessage, Helper } from "./helper";
import * as detect from 'detect-browser'

// Leider will Safari es anderes als die Anderen...
let LINK = "href"
const browser = detect.detect()
if (browser && (browser.name === 'safari' || browser.name === 'ios')) {
  LINK = "xlink:href"
}

@autoinject
@noView
export class PushButton2 implements Component {
  @bindable cfg
  body: any;
  component_name: "Push Button"
  private state = "off"
  private button;

  constructor(private hlp: Helper, public element: Element) { }
  attached() {
    this.hlp.initialize(this, {
      size: 110,
      message: "pushbutton",
      caption: "drück mich"
    })
  }

  configure() {
  }

  render() {
    const dim = this.hlp.defaultFrame(this)
    this.button = this.body.append("svg:image")
      .attr(LINK, "img/off_button.png")
      .attr("x", dim.x)
      .attr("y", dim.y)
      .attr("width", dim.w)
      .attr("height", dim.h)
      .on("click", event => {
        this.hlp.ea.publish(this.cfg.message, 
          { state: (this.state == "on") ? "off" : "on" ,
          direction: "out"})
      })

  }

  update(val: eaMessage) {
    const img = val.data.state === "on" ? "img/on_button.png" : "img/off_button.png"
    this.state = val.data.state
    this.button.attr(LINK, img)
  }

}
