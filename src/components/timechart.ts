/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { select, Selection } from 'd3-selection'
import { scaleLinear, scaleTime } from "d3-scale";
import { axisBottom, axisLeft, axisRight } from 'd3-axis'
import {mean,extent} from 'd3-array'
import { line as lineGenerator, curve, curveBundle, curveCatmullRom } from 'd3-shape'
import { format as numberFormat } from 'd3-format'
import { timeFormat, timeFormatLocale } from 'd3-time-format'
import { Dataloader } from '../services/dataloader'

import { Helper, Component, eaMessage } from './helper'
import { Util } from '../services/util';

/**
 * Datentp für die Definition einer Achse festlegen.
 */
export type axis = {
  caption: string
  minValue: number | Date
  maxValue: number | Date
}

/**
 * Die Timechart bildet Werte auf Zeitpunkte innerhalb des gewünschten Zeitraums ab.
 *  Es kann entweder nur eine Kurve mit den Werten auf der linken Y-Achse, oder zwei Kurven mit unterschiedlichen Wertebereichen auf beiden Y-Achsen gezeichnet werden.
 */
@autoinject
@noView
export class Timechart implements Component {
  @bindable cfg
  component_name: "Timechart"
  body: Selection
  private chart
  private xAxis
  private yLeft
  private yRight
  private dateFormat = timeFormat("%H:%M")
  private tooltip
  private tooltipText
  private scales = {
    x: null,
    yl: null,
    yr: null
  }


  constructor(private hlp: Helper, public element: Element,
    private dl: Dataloader) { }

  configure() { }

  attached() {
    this.hlp.initialize(this, {
      width: 300,
      height: 150,
      message: "timechart_redraw",
      y_left: <axis>{
        caption: "Links",
        minValue: 0,
        maxValue: 100
      },
      time: <axis>{
        caption: "X-Achse",
        minValue: new Date().getTime(),
        maxValue: new Date().getTime() + 86400000
      },
      left_offset: 25,
      right_offset: 25,
      bottom_offset: 25

    })
  }

  render() {
    const dim = this.hlp.defaultFrame(this)
    const captionfontsize = 14

    const left = this.cfg.left_offset
    const right = this.cfg.right_offset
    const bottom = this.cfg.bottom_offset

    this.chart = this.body.append("svg:g")
      .attr("transform", `translate(${Helper.BORDER},${dim.y})`)
      .attr("width", dim.w + "px")
      .attr("height", dim.h + "px")

    // Scale für X-Achse
    this.scales.x = scaleTime()
      .range([left, dim.w - right])
      .clamp(true)

    // Scale für die linke Y-Achse
    this.scales.yl = scaleLinear()
      .domain([this.cfg.y_left.minValue, this.cfg.y_left.maxValue])
      .range([dim.h - bottom, dim.y - bottom])

    // Darstellung der X- und Y-Achsen  
    this.xAxis = axisBottom()
    // Darstellung der Y-Achse
    this.yLeft = axisLeft().scale(this.scales.yl)

    // SVG Objekte, die die Achsen aufnehmen
    this.chart.append('g')
      .attr("transform", `translate(0,${dim.h - bottom})`)
      .attr("class", "xaxis")
    this.chart.append('g')
      .attr("transform", `translate(${left},0)`)
      .attr("class", "yleft")
      .call(this.yLeft)
    this.chart.append("text")
      .attr("x", dim.x + left)
      .attr("y", captionfontsize)
      .attr("text-anchor", "start")
      .style("font-size", captionfontsize)
      .style("fill", "red")
      .text(this.cfg.y_left.caption)


    // SVG Path für die Diagramm-Linien
    this.chart.append("path")
      .classed("lineleft", true)

    // Rechte Y-Achse, nur wenn Darstellung gewünscht wird  
    if (this.cfg.hasOwnProperty("y_right")) {
      this.scales.yr = scaleLinear()
        .domain([this.cfg.y_right.minValue, this.cfg.y_right.maxValue])
        .range([dim.h - bottom, dim.y - bottom])
      this.yRight = axisRight().scale(this.scales.yr)
      this.chart.append('g')
        .attr("transform", `translate(${dim.w - right},0)`)
        .attr("class", "yright")
        .call(this.yRight)
      this.chart.append("text")
        .attr("x", dim.w - right - 4)
        .attr("y", captionfontsize)
        .attr("text-anchor", "end")
        .style("font-size", captionfontsize)
        .style("fill", "blue")
        .text(this.cfg.y_right.caption)

      this.chart.append("path")
        .classed("lineright", true)

    }

    this.tooltip = this.chart.append("svg:g")
      .style("opacity", 0)
    this.hlp.rectangle(this.tooltip, 0, 0, 115, 40, "tooltip")
      .attr("rx", "10px")
      .attr("ry", "10px")
    this.tooltipText = this.hlp.stringElem(this.tooltip, 5, 15, 14, "start")
    this.redraw()

  }

  update(val: eaMessage) {
    //interessiert uns nicht.
    //this.redraw();
  }

  redraw() {
    let points = this.cfg.devices.map(device => { return { id: device } })
    points[0].definition = this.cfg.y_left
    if (points.length > 1) {
      points[1].definition = this.cfg.y_right
    }
    this.dl.getSeries(null, null, points).then(result => {
      if (result[points[0].id] != undefined && this.cfg.hasOwnProperty("y_left")) {
        const leftData = result[points[0].id]
        this.scales.x.domain(extent(leftData, d => d[0]))
        this.xAxis.scale(this.scales.x)
          .tickFormat(this.dateFormat
          )
        this.chart.select(".xaxis").call(this.xAxis)
        this.doDrawLine(leftData, "left", this.scales.yl)

      }
      if (points[1] && (result[points[1].id] != undefined) && (this.cfg.hasOwnProperty("y_right"))) {
        const rightData = result[points[1].id]
        this.doDrawLine(rightData, "right", this.scales.yr)
      }
    })
  }

  doDrawLine(data: Array<Array<any>>, axis: string, scale: scaleLinear) {
    const conf = this.cfg["y_" + axis]
    const nf = numberFormat(`.${conf.precision ? conf.precision : 0}f`)
    const line = lineGenerator()
      .x(d => this.scales.x(parseInt(d[0])))
      .y(d => scale(parseFloat(d[1])))
      .curve(curveCatmullRom.alpha(0.7))
    this.chart.select(`.line${axis}`).datum(data).attr("d", line)
    while (data.length > 24) {
      data = Util.arrayCombine(data, (a, b) => {
        return [mean([a[0], b[0]]), mean([a[1], b[1]])]
      })
    }
    data.forEach(d => {
      const x = this.scales.x(d[0])
      const y = scale(d[1])
      this.chart.append("circle")
        .attr("r", 5)
        .attr("cx", x)
        .attr("cy", y)
        .classed(`dot${axis}`, true)
        .lower()
        .on('mouseover', () => {
          this.tooltipText.text(this.dateFormat
            (d[0]) + " Uhr: " + nf(parseFloat(d[1])) + conf.suffix)
          const box = this.tooltipText.node().getBBox()

          const r = this.tooltip.select("rect")
          const ttw = box.width + 10
          r.attr("width", ttw)
          const tth = parseInt(r.attr("height"))
          const maxw = parseInt(this.chart.attr("width"))
          const maxh = parseInt(this.chart.attr("height"))
          const ttx = Math.min(x, maxw - ttw)
          const tty = Math.min(y, maxh - tth)
          this.tooltip
            .attr("transform", `translate(${ttx},${tty})`)
            .transition()
            .duration(300)
            .style("opacity", 0.9)

        })
        .on('mouseout', () => {
          this.tooltip.transition()
            .duration(500)
            .style("opacity", 0)
        })
    })
  }

}
