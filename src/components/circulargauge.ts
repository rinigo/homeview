/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { EventAggregator } from "aurelia-event-aggregator"
import { scaleLinear } from 'd3-scale'
import { select, Selection } from 'd3-selection'
import 'd3-transition'
import { Helper, Component, eaMessage } from './helper'

@autoinject
@noView
export class CircularGauge implements Component{
  @bindable cfg;
  component_name="CircularGauge"
  private scale
  private arcsize
  private pointer
  private rotation = () => 360 - (this.cfg.MAX_ANGLE - this.cfg.MIN_ANGLE) / 2
  body
  private valueText

  constructor(private hlp: Helper, public element: Element, 
    private ea: EventAggregator) { }

  configure() {
    this.scale = scaleLinear()
      .domain([this.cfg.min, this.cfg.max])
      .range([this.cfg.MIN_ANGLE, this.cfg.MAX_ANGLE])
    this.scale.clamp(true)
    this.arcsize = this.cfg.size / 7

  }

  attached() {
    this.hlp.initialize(this, {
      size: 150,
      min: 0,
      max: 100,
      MAX_ANGLE: 300,
      MIN_ANGLE: 0,
      message: "temperatur",
      bands: [{ from: 0, to: 100, color: "blue" }]
    })
  }

  // initial display of the component
  render() {
    // basic setup
    let dim = this.hlp.defaultFrame(this)
    let center_x = dim.x+dim.w / 2
    let center_y = dim.y+dim.h / 2
    let size = (dim.w / 2) * 0.8
 
    const pointer_width = 10
    const pointer_base = 0.3
    const pointer_stroke = `M ${-size * pointer_base} 0
    L 0 ${-pointer_width / 2}
    L ${size * (1 - pointer_base)} 0
    L 0 ${pointer_width / 2}
    Z`

    /*
      Draw the coloured bands for the scale
    */
    this.cfg.bands.forEach(band => {
      this.hlp.arch(this.body, center_x, center_y, size - this.arcsize, size,
        this.hlp.deg2rad(this.scale(band.from)),
        this.hlp.deg2rad(this.scale(band.to)), band.color, this.rotation())
    })

    // Draw the pointer
    //this.pointer = this.hlp.line(this.body, center, center, center, center - size, "red", 2)
    let pframe = this.body.append("g")
    .attr("transform", `translate(${center_x},${center_y}) rotate(${this.rotation()-90})`)
    this.pointer = pframe.append("g")
    this.pointer.append("svg:path")
      .attr("d", pointer_stroke)
      .classed("pointer", true)
    this.pointer.append("svg:circle")
      .attr("cx", 0)
      .attr("cy", 0)
      .attr("r", 8)


    /* field for actual measurement */
    let valuesFontSize = Math.round(size / 4)
    this.valueText = this.hlp.stringElem(this.body, center_x, center_y + size / 2,
      valuesFontSize, "middle")

    /* create tickmarks  */
    let tickmarkFontSize=this.arcsize/3
    this.scale.ticks(15).forEach(tick => {
      let p1 = this.valueToPoint(tick, 1.0)
      let p2 = this.valueToPoint(tick, 0.8)
      let p3= this.valueToPoint(tick,1.15)
      this.hlp.line(this.body, center_x - p1.x, center_y - p1.y, center_x - p2.x, center_y - p2.y, "black", 1.2)
      this.hlp.stringElem(this.body,center_x-p3.x,center_y-p3.y,tickmarkFontSize,"middle").text(tick)
    })

    this.update({message:"",data:0})
  }

  // called if new value arrives
  update(value:eaMessage) {
   
    this.pointer
      .transition()
      .duration(300)
      .attr("transform",`rotate(${this.scale(value.data)})`)

    this.valueText.text(value.data)
  }

  valueToPoint(val, factor) {
    let arc = this.scale(val) + 90 + this.rotation()
    let rad = this.hlp.deg2rad(arc)
    let r = ((this.cfg.size / 2) * 0.9 - this.arcsize) * factor
    let x = r * Math.cos(rad)
    let y = r * Math.sin(rad)
    return { x, y }
  }
}