/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { scaleLinear } from 'd3-scale'
import { select, Selection } from 'd3-selection'
import 'd3-transition'
import { Helper, Component, eaMessage } from './helper'

const MIN_ANGLE = 15
const MAX_ANGLE = 165

@autoinject
@noView
export class DoubleGauge implements Component{
  @bindable cfg;
  component_name="DoubleGauge";

  private arcsize;
  private upperScale
  private lowerScale
  private upperPointer
  private lowerPointer
  private upperValueText
  private lowerValueText
  body

  constructor(private hlp: Helper, public element: Element) { }

  configure() {
    this.upperScale = scaleLinear()
      .domain([this.cfg.upper.minValue, this.cfg.upper.maxValue])
      .range([MIN_ANGLE, MAX_ANGLE])
    this.upperScale.clamp(true)

    this.lowerScale = scaleLinear()
      .domain([this.cfg.lower.minValue, this.cfg.lower.maxValue])
      .range([MAX_ANGLE, MIN_ANGLE])
    this.lowerScale.clamp(true)

    this.arcsize = this.cfg.size / 7
  }

  attached() {
    this.hlp.initialize(this, {
      size: 150,
      upper: {
        minValue: 0,
        maxValue: 100,
        bands: [{ from: 0, to: 100, color: "blue" }],
      },
      lower:{
        minValue:0,
        maxValue: 100,
        bands: [{ from: 0, to: 100, color: "green" }]
      },

      message: ["doublegauge_upper_value", 
                "doublegauge_lower_value"],

    })
  }

  render() {
    let dim = {w:this.cfg.size,h:this.cfg.size}
    const y_offset=this.hlp.frame(this.body, dim.w, dim.h, this.cfg.caption, dim.h/10)
    dim={w:dim.w-y_offset,h:dim.h-y_offset}
    let center = {x: dim.w / 2+y_offset/2,y: dim.h/2+y_offset}
    let size = {w: (dim.w / 2) * 0.9,h: (dim.h/2)*0.9}

    const pointer_width = 10
    const pointer_base = 0.3
    const pointer_stroke = 
     `M ${-size.w * pointer_base} 0
      L 0 ${-pointer_width / 2}
      L ${size.w * (1 - pointer_base)} 0
      L 0 ${pointer_width / 2}
      Z`


   
    /*
      Draw the coloured bands for the scale
    */
    let drawBands=(bands,scale,angle)=>{
      bands.forEach(band=>{
        this.hlp.arch(this.body, center.x, center.y, 
          size.w - this.arcsize, size.w,
          this.hlp.deg2rad(scale(band.from)),
          this.hlp.deg2rad(scale(band.to)), band.color, angle)
          
      })
    }
    drawBands(this.cfg.upper.bands,this.upperScale,270)
    drawBands(this.cfg.lower.bands,this.lowerScale,90)
  
    // Draw the pointers
    let pframe = this.body.append("g")
      .attr("transform", 
          `translate(${center.x},${center.y}) rotate(180)`)

    this.upperPointer=pframe.append("svg:path")
      .attr("d", pointer_stroke)
      .classed("pointer", true)

    this.lowerPointer = pframe.append("svg:path")
      .attr("d", pointer_stroke)
      .classed("pointer", true)

    pframe.append("svg:circle")
      .attr("cx", 0)
      .attr("cy", 0)
      .attr("r", 10)


    /* field for actual measurement */
    let valuesFontSize = Math.round(size.h / 5)
    this.upperValueText = this.hlp.stringElem(this.body, center.x,
      center.y - size.h / 2, valuesFontSize, "middle")
    this.lowerValueText = this.hlp.stringElem(this.body, center.x,
      center.y + size.h / 2, valuesFontSize, "middle")

    /* create tickmarks  */
    let tickmarkFontSize = this.arcsize / 3
    let createTickmarks = (scale, f) => {
      scale.ticks().forEach(tick => {
        const valueToPoint=(val, factor, scale)=>{
          let arc = scale(val)
          let rad = this.hlp.deg2rad(arc)
          let r = ((dim.w / 2) * 0.9 - this.arcsize) * factor
          let x = r * Math.cos(rad)
          let y = r * Math.sin(rad)
          return { x, y }
        }
        let p1 = valueToPoint(tick, 1.2, scale)
        let p2 = valueToPoint(tick, 1.0, scale)
        let p3 = valueToPoint(tick, 1.35, scale)
        this.hlp.line(this.body, center.x + p1.x * f, center.y + p1.y * f,
          center.x + p2.x * f, center.y + p2.y * f, "black", 1.2)
        this.hlp.stringElem(this.body, center.x + p3.x * f, center.y + p3.y * f,
          tickmarkFontSize, "middle").text(tick)
      })

    }
    createTickmarks(this.upperScale, -1)
    createTickmarks(this.lowerScale, 1)

    this.upperRedraw(0)
    this.lowerRedraw(0)

    
  }

  update(newVal: eaMessage){
    if(newVal.message === this.cfg.message[0]){
      this.upperRedraw(newVal.data)
    }else{
      this.lowerRedraw(newVal.data)
    }
  }

  upperRedraw(upper) {
    if(isNaN(upper)){
      this.upperValueText.text("Error");
    }else{
    this.upperPointer
      .transition()
      .duration(600)
      .attr("transform", `rotate(${this.upperScale(upper)})`)
      this.upperValueText.text(upper+this.cfg.upper.suffix)

    }
 
  }
  lowerRedraw(lower) {
    this.lowerPointer
      .transition()
      .duration(600)
      .attr("transform", `rotate(${180 + this.lowerScale(lower)})`)
    this.lowerValueText.text(lower+this.cfg.lower.suffix)
  }

  
}