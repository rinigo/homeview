/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { EventAggregator, configure } from "aurelia-event-aggregator"
import { scaleLinear } from 'd3-scale'
import { select, Selection } from 'd3-selection'
import 'd3-transition'
import { Helper, Component, eaMessage } from './helper'

/**
 * Die Nachricht, die der TristateButton über den EventAggregator versendet und empfängt
 */
export interface tristateMessage {
  direction: "in" | "out"
  state: "on" | "off"
  mode: "auto" | "manual"
  power: number
}


/**
 * Ein TristateButton ist ein Knopf, der auf "ein", "aus" oder "automatik" geschaltet werden kann.
 */
@autoinject
@noView
export class TristateButton implements Component{
  @bindable cfg
  component_name="TristateButton"
  private mode = "auto"
  private state = "on";
  private power;
  body
  private upper;
  private lower;
  private autoText;
  private powerText;

  constructor(private hlp: Helper, public element: Element,
    private ea: EventAggregator) { }

  configure() {}

  attached() {
    this.hlp.initialize(this,{
      size: 110
    })

  }

  /*
  update(val:eaMessage){
    let newState=val.data
    if(newState.state) this.state = newState.state
    if(newState.mode) this.mode = newState.mode
    if(newState.power) this.power = newState.power;
    this.set()
 

  }

  */
  render() {
    const dim=this.hlp.defaultFrame(this)
    const ratio=0.6
  
    this.upper = this.hlp.rectangle(this.body, dim.x, 
                    dim.y, dim.w, dim.h, "light_on")
      .attr("rx", 15)
      .attr("ry", 15)
      .attr("clip-path", "url(#clip-bottom)")
      .on("click", () => {
        this.ea.publish(this.cfg.message, {
          state: this.state === "on" ? "off" : "on",
          mode: "manual",
          direction: "out"
        })
      })

    let fontsize = Math.floor(dim.h / 5)
    let text_cx = this.cfg.size / 2
    let text_y = Math.round(dim.y+dim.h*ratio+(dim.h*(1-ratio))/2)-1
    let txtgroup = this.body.append("svg:g")
      .attr("transform", `translate(${text_cx},${text_y})`)
    this.autoText = this.hlp.stringElem(txtgroup, 0, 0, fontsize, "middle")
      .text("auto")
      
    this.powerText=this.hlp.stringElem(this.body,text_cx,dim.h/2,fontsize,"middle",0)
      .style("pointer-events","none");

    this.lower = this.hlp.rectangle(this.body, dim.x, 
                    dim.y, dim.w, dim.h, "mode_auto")
      .attr("rx", 15)
      .attr("ry", 15)
      .attr("clip-path", "url(#clip-top)")
      .on("click", () => {
        this.ea.publish(this.cfg.message, {
          state: this.state,
          mode: this.mode == "auto" ? "manual" : "auto",
          direction:"out"
        })
      })


    const defs = this.body.append("svg:defs")
    defs.append("svg:clipPath")
      .attr("id", "clip-bottom")
      .append("svg:rect")
      .attr("x", dim.x)
      .attr("y", dim.y)
      .attr("width", dim.w)
      .attr("height", dim.h*ratio)

    defs.append("svg:clipPath")
      .attr("id", "clip-top")
      .append("svg:rect")
      .attr("x", dim.x)
      .attr("y", dim.y+dim.h*ratio)
      .attr("width", dim.w)
      .attr("height", dim.h * (1-ratio) )

  }

  update(val:eaMessage) {
    let newstate=val.data
  //  console.log(val.message+", "+JSON.stringify(val.data))
    let bActState = (newstate.state === "on")
    let bActMode = (newstate.mode === "auto")
    this.upper
      .classed("light_on", bActState)
      .classed("light_off", !bActState)
    this.lower
      .classed("mode_manual", !bActMode)
      .classed("mode_auto", bActMode)
    this.autoText.attr("opacity", bActMode ? 1.0 : 0.2)
    if(newstate.power){
      let tx=newstate.power
      if(newstate.power>1000){
        tx=Math.round(newstate.power)
      }else if(newstate.power>100){
        tx=Math.round(10*newstate.power)/10
      }else{
        tx=Math.round(100*newstate.power)/100
      }
      this.powerText.text(tx+" W")
    }
    this.state=newstate.state
    this.mode=newstate.mode
  }

}