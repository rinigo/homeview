/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/
import { bindable, autoinject } from 'aurelia-framework'
import { EventAggregator } from 'aurelia-event-aggregator'

@autoinject
export class PushButton{
@bindable cfg
@bindable pressed:boolean;

  constructor(private ea:EventAggregator){}

  attached(){
    this.cfg=Object.assign({},{
      message: "pushbutton"
    },this.cfg)
  }

  toggle(){
    this.pressed=!this.pressed
    this.ea.publish(this.cfg.message,this.pressed)
  }
}
