import { sliderHorizontal } from 'd3-simple-slider'
import { bindable, noView, autoinject } from 'aurelia-framework'
import { Helper, Component, eaMessage } from './helper'
import { Util } from '../services/util';

@autoinject
@noView
export class Setpoint implements Component {
  @bindable cfg
  body: any;
  component_name = "Setpoint";
  private slider

  constructor(private hlp: Helper, public element: Element) { }

  attached() {
    this.hlp.initialize(this, {
      minValue: 0,
      maxValue: 100,
      step: 1,
      width: 250,
      height: 50,
      displayValue: true,
      message: "setpoint"
    })
  }
  configure() { }
  render() {
    let dim = this.hlp.defaultFrame(this)
    let debounced = Util.debounce((val) => {
      this.hlp.ea.publish(this.cfg.message, val)
    }, 200, this)
    this.slider = sliderHorizontal()
      .min(this.cfg.minValue)
      .max(this.cfg.maxValue)
      .step(this.cfg.step)
      .width(dim.w - 2 * Helper.BORDER - 2 * this.cfg.offset)
      .displayValue(this.cfg.displayValue)
      .on('onchange', debounced)

    this.body.append("g")
      .attr("width", dim.w)
      .attr("height", this.cfg.height)
      .attr("transform", `translate(${dim.x + Helper.BORDER + this.cfg.offset},${dim.y + Helper.BORDER + this.cfg.offset})`)
      .call(this.slider)
  }

  update(value: eaMessage) {
    if (value && value.data) {
      this.slider.value(value.data)
    }
  }

}
