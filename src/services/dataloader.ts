/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { Util } from './util'
import { max, mean, merge, range } from 'd3-array'
import { scaleLinear } from 'd3-scale'
import { FetchService } from "./fetchservice";
import env from '../environment';
import { autoinject } from 'aurelia-framework';
import * as moment from 'moment'


@autoinject
export class Dataloader {
 
  constructor(private fetcher: FetchService) { }

/**
  * Read new data from an influx database (as defined in global.influx)
  * @param from -  start timestamp (inlcuding)
  * @param to  - end timestamp (excluding
  * If from is null: select a 24-h-period ending "now".
  * if from is given and to is null: Select a 24-h-period starting at from.
  * @returns {Promise<{}>}
**/

  async getSeries(from: Date, to: Date, datapoints: Array<any>) {
    if (!from) { 
      from = moment().subtract(1, 'd').toDate()
      to = new Date();
    }
    if (!to) {
      to = moment(from).add(1, 'd').toDate()
    }

    const ret = {}
    const from_ms = from.getTime();
    const to_ms = to.getTime();

    if (env.mock) {
      let dummydata = (f, t, domain) => {
        if(domain == undefined){
          domain=[0,1000]
        }  
        let scale=scaleLinear().domain([f,t]).range(domain)
        return range(f,t,3000000).map(item => { // alle 10 Minuten
          return [item, ((domain[1]-domain[0])*Math.random()+domain[0])]
        })
      }
      datapoints.forEach(dp => {
        ret[dp.id] = dummydata(from_ms, to_ms, [dp.definition.minValue,dp.definition.maxValue])
      });
      return ret;
    } else {
      let query = "";
      datapoints.forEach(dp => {
        query += `select value from "${dp.id}" where time > ${from.getTime()}ms and time < ${to.getTime()}ms;`
      })
      // console.log("querying "+query)
      const sql = Util.urlencode(query)
      const raw = await this.fetcher
        .fetchJson(`${env.influx}/query?db=iobroker&epoch=ms&precision=ms&q=${sql}`)
      const ret = {}
      // console.log("raw result: "+JSON.stringify(raw))
      raw.results.forEach(result => {
        if (result.series) {
          result.series.forEach(serie => {
            ret[serie.name] = serie.values
          })
        }
      })
      return ret
    }
  }
}
