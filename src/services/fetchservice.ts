/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { HttpClient } from 'aurelia-fetch-client'
import env from '../environment'

export class FetchService {
  private http = new HttpClient()

  /**
   * Eine JSON Antwort von einer URL holen, oder einen
   * zufälligen Mock-Wert, falls environment.mock== true ist, oder
   * die URL mit fake:// anfängt.
   * @param url die URL
   */
  public async fetchJson(url) {
    if (env.mock || url.startsWith("fake")) {
      let upper = 100
      let lower = -10
      KNOWN_SOURCES.find(source => {
        if (url.toLowerCase().includes(source.s)) {
          upper = source.u
          lower = source.l
          return true
        } else {
          return false
        }
      })
      let r = Math.random()
      return { val: Math.round((r * (upper - lower) + lower) * 10) / 10 }
    } else {
      let raw = await this.http.fetch(url)
      return await raw.json()
    }
  }

  /* 
    Das .val Attribut der Antwort einer Anfrage liefern.
  */
  public async fetchJsonVal(url) {
    let result = await this.fetchJson(url)
    return result.val
  }

  /**
   * Einen ioBroker State auslesen
   * @param id is des States
   */
  public async getIobrokerValue(id) {
    const encoded = encodeURIComponent(id)
    return await this.fetchJsonVal(`${env.iobroker}/get/${encoded}`)
  }

  public async setIoBrokerValue(id, value) {
    const encoded = encodeURIComponent(id)
    if (env.mock) {
      return {
        "id": id,
        "value": value
      }
    } else {
      let raw = await this.http.fetch(`${env.iobroker}/set/${encoded}?value=${value}`)
      let result = await raw.json();
      // console.log("sent " + JSON.stringify(result))
      return result
    }
  }
  /**
   * Mehrere ioBroker States auslesen und zusammen zurückliefern
   * @param ids die IDs der gesünschten States
   */
  public getIobrokerValues(ids: Array<String>): Promise<Array<any>> {

    return Promise.all(ids.map(id => {
      const encoded = encodeURIComponent(id as string)
      return this.getIobrokerValue(encoded)
    })).then(result => {
      return result
    })
  }
}

const KNOWN_SOURCES = [
  { s: "temperatur", l: 10, u: 30 },
  { s: "humid", l: 30, u: 70 },
  { s: "energy", l: 1000, u: 100000 },
  { s: "power", l: 0, u: 10000 },
  { s: "012", l: 0, u: 2 },
  { s: "manuell", l: 0, u: 2 },
  { s: "cent", l: 0, u: 100 },
  { s: "luftdruck", l: 960, u: 1050 },
  { s: "pressure", l: 960, u: 1050 }
]

