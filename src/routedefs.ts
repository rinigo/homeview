/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/


import { Router, RouterConfiguration} from 'aurelia-router'
import 'bootstrap'
import { autoinject } from 'aurelia-framework';
import configs from './config'

export class App {
  public router: Router;
  private conf=configs
  public configureRouter(config: RouterConfiguration, router: Router) {
    this.router = router
    config.map([
      {
        route: ["","overview"],
        name: "overview",
        moduleId: 'app',
        title: "Übersicht",
        nav: true
      }, {
        route: "oben",
        moduleId: "routes/oben",
        name: "oben",
        title: "Obergeschoss",
        nav: true
      }, {
        route: "tech",
        moduleId: "routes/tech",
        name: "tech",
        title: "Haustechnik",
        nav: true
      }
    ])
  }
}
