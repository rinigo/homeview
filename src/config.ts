/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import env from './environment'
const gauge_size = 242;
const switch_size = 80;

const climate = {
  temperature: {
    caption: "Temperatur",
    suffix: "°C",
    minValue: 17,
    maxValue: 35,
    precision: 1
  },
  humidity: {
    caption: "Luftfeuchte",
    suffix: "%",
    minValue: 20,
    maxValue: 80,
    precision: 0
  },
  temp_scale: {
    bands: [{ from: 10, to: 17, color: "#8cf2e4" },
    { from: 15, to: 20, color: "yellow" },
    { from: 20, to: 26, color: "green" },
    { from: 26, to: 35, color: "red" }],
    minValue: 15,
    maxValue: 35,
    suffix: "°C",
    precision: 1
  },
  humid_scale: {
    bands: [{ from: 20, to: 40, color: "yellow" },
    { from: 40, to: 60, color: "green" },
    { from: 60, to: 80, color: "yellow" }],
    minValue: 20,
    maxValue: 80,
    suffix: "%",
    precision: 0
  }

}

export default {
  "SWITCH_ON": 1,
  "SWITCH_OFF": 0,
  "SWITCH_AUTO": 2,
  powermeter_cfg: {
    devices: [env.devices.energy_grid_flow],
    width: gauge_size,
    height: switch_size,
    padding: 12,
    minValue: -10000,
    maxValue: 10000,
    caption: "Netto Strom",
    message: "fronius_net",
    bands: [
      { from: -10000, to: 0, color: "red" },
      { from: 0, to: 10000, color: "green" }
    ],
    suffix: "W",
    modify: x => -Math.round(x)

  },
  wohnzimmertemp_cfg: {
    "devices": [env.devices.wohnzimmer_temp, env.devices.wohnzimmer_hygro],
    "size": gauge_size,
    upper: climate.temp_scale,
    lower: climate.humid_scale,
    message: ["wohnzimmer_temp", "wohnzimmer_hygro"],
    caption: "Wohnzimmer",
    visible: true
  },
  wohnzimmer_chart_cfg: {
    devices: [env.devices.wohnzimmer_temp, env.devices.wohnzimmer_hygro],
    width: 2 * gauge_size,
    height: gauge_size,
    y_left: climate.temperature,
    y_right: climate.humidity,
    message: "chart_aussentemp",
    caption: "Verlauf Wohnzimmer",
    visible: false
  },
  dusche_cfg: {
    "devices": [env.devices.dusche_temp, env.devices.dusche_hygro],
    "size": gauge_size,
    upper: climate.temp_scale,
    lower: climate.humid_scale,
    message: ["dusche_temp", "dusche_hygro"],
    caption: "Dusche",
    visible: true
  },
  dusche_chart_cfg: {
    devices: [env.devices.dusche_temp, env.devices.dusche_hygro],
    width: 2 * gauge_size,
    height: gauge_size,
    y_left: climate.temperature,
    y_right: climate.humidity,
    message: "chart_aussentemp",
    caption: "Verlauf Dusche",
    visible: false
  },
  bad_oben_cfg: {
    "devices": [env.devices.bad_oben_temp, env.devices.bad_oben_hygro],
    "size": gauge_size,
    upper: climate.temp_scale,
    lower: climate.humid_scale,
    message: ["bad_oben_temp", "bad_oben_hygro"],
    caption: "Bad",
    visible: true
  },
  bad_oben_chart_cfg: {
    devices: [env.devices.bad_oben_temp, env.devices.bad_oben_hygro],
    width: 2 * gauge_size,
    height: gauge_size,
    y_left: climate.temperature,
    y_right: climate.humidity,
    message: "chart_aussentemp",
    caption: "Verlauf Bad oben",
    visible: false
  },
  dachstock_cfg: {
    "devices": [env.devices.dachstock_temp, env.devices.dachstock_hygro],
    "size": gauge_size,
    upper: climate.temp_scale,
    lower: climate.humid_scale,
    message: ["dachstock_temp", "dachstock_hygro"],
    caption: "Dach",
    visible: true
  },
  dachstock_chart_cfg: {
    devices: [env.devices.dachstock_temp, env.devices.dachstock_hygro],
    width: 2 * gauge_size,
    height: gauge_size,
    y_left: climate.temperature,
    y_right: climate.humidity,
    message: "chart_aussentemp",
    caption: "Verlauf Dachstock",
    visible: false
  },
  barometer_cfg: {
    devices: env.devices.barometer,
    size: gauge_size,
    min: 980,
    max: 1040,
    message: "luftdruck",
    caption: "Barometer",
    bands: [{ "from": 975, "to": 995, color: "#f2a44b" }, { from: 995, to: 1025, color: "#42d4f4" }, { from: 1025, to: 1045, color: "#f2a44b" }]
  },
  barometer_chart_cfg: {
    devices: [env.devices.barometer],
    width: 2 * gauge_size,
    height: gauge_size,
    y_left: {
      caption: "Luftdruck",
      minValue: 960,
      maxValue: 1040,
      suffix: " mbar",

    },
    message: "chart_barometer",
    caption: "Verlauf Luftdruck",
    left_offset: 35,
    visible: false
  },
  aussentemp_cfg: {
    "devices": [env.devices.aussen_temp, env.devices.aussen_hygro],
    "size": gauge_size,
    upper: {
      bands: [{ from: -15, to: 0, color: "#8cf2e4" },
      { from: 0, to: 10, color: "yellow" },
      { from: 10, to: 25, color: "green" },
      { from: 25, to: 40, color: "red" }],
      minValue: -15,
      maxValue: 40,
      suffix: "°C",
      precision: 1
    },
    lower: {
      bands: [{ from: 20, to: 40, color: "yellow" },
      { from: 40, to: 60, color: "green" },
      { from: 60, to: 80, color: "yellow" }],
      minValue: 20,
      maxValue: 80,
      suffix: "%",
      precision: 0
    },
    message: ["aussen_temp", "aussen_hygro"],
    caption: "Aussen",
    visible: true
  },
  aussen_chart_cfg: {
    devices: [env.devices.aussen_temp, env.devices.aussen_hygro],
    width: 500,
    height: gauge_size,
    y_left: {
      caption: "Temperatur",
      suffix: "°C",
      minValue: -10,
      maxValue: 35,
      precision: 0
    },
    y_right: climate.humidity,
    message: "chart_aussentemp",
    caption: "Verlauf draußen",
    visible: false
  },
  carloader_cfg: {
    "state_id": env.devices._car_loader_state,
    "mode_id": env.devices._car_loader_manual,
    // "power_id": env.devices._car_loader_power,
    "size": switch_size,
    "height": switch_size,
    "message": "car_loader",
    "caption": "Auto"
  },
  pushbutton_cfg: {
    message: "treppenlicht",
    size: switch_size
  },
  mediacenter_cfg: {
    state_id: env.devices._mediacenter_state,
    size: switch_size,
    message: "mediacenter_switch",
    caption: "Mediacenter"
  },
  extender_cfg: {
    state_id: env.devices._wlan_state,
    size: switch_size,
    message: "wlan_extender_switch",
    caption: "Schalter 2"
  },

  e14_cfg: {
    size: switch_size,
    state_id: "lightify.0.580C0C0000261884.on",
    caption: "Büro Licht",
    message: "e14state"
  },
  aussenlicht_cfg: {
    "state_id": env.devices.treppenlicht_direkt,
    "mode_id": env.devices.treppenlicht_modus,
    "size": switch_size,
    "message": "aussenlicht",
    "caption": "Außenlicht"
  },
  fernsehlicht_cfg: {
    "state_id": env.devices.fernsehlicht_direkt,
    "mode_id": env.devices.fernsehlicht_modus,
    "size": switch_size,
    "message": "fernsehlicht",
    "caption": "TV-Licht"
  },
  "boiler_cfg": {
    "minValue": 30,
    "maxValue": 80,
    "step": 5,
    "width": gauge_size,
    "height": switch_size,
    "caption": "Boilertemperatur",
    "offset": 5,
    "message": "boiler_temp_set"
  }
}
