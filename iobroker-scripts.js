/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2017                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

// !1!
log("Hello, World", "info")
// /!1!

// !2!
setState("lightify.0.663254887.on", true);
// /!2!

// !3!
var esszimmerlicht = "lightify.0.64EADA0002261884"
var korridorlicht = "hue.0.Philips_hue.Korridor"
var esszimmer_an_aus = esszimmerlicht + ".on"
var korridor_an_aus = korridorlicht + ".on"
on({ id: esszimmer_an_aus }, function () {
  var State = getState(esszimmer_an_aus).val
  setState(korridor_an_aus, state)
})
// /!3!

// !4!
var obj = {
  attribut1: "die Antwort",
  attribut2: 42,
  attribut3: false,
  attribut4: [1, 2, 3],
  attribut5: { name: "Rumpelstilzchen" }
}
// /!4!

// !5!
var licht = {
  on: true,
  bri: 70,
  r: 255,
  g: 255,
  b: 125,
  reachable: true
}
// /!5!

// !6!

createState("aussenlicht_manuell", 2)

var _treppenlicht = "lightify.0.904AA200AA3EB07C.on"



/**
 * Treppenlicht ein oder ausschalten. Wir führen den Schaltauftrag nur aus, 
 * wenn das Licht auf "Automatik" geschaltet ist, und wenn es nicht sowieso
 * schon den angeforderten Zustand hat.
 * @param conditional: Nur schalten, wenn auf "auto" gestellt, sonst ignorieren
 * @param mode: true: ein, false: aus
 */
function treppenlicht(conditional, mode) {
  log("treppenlicht: " + (mode ? "ein" : "aus"), "debug");

  if (conditional) {
    if (getState("aussenlicht_manuell").val != 2) {
      log("function denied. We're in manual mode", "info")
      return
    }
  }
  if (getState(_treppenlicht).val != mode) {
    setState(_treppenlicht, mode)
  }

}

/**
 * Je nach Tageszeit ein oder ausschalten.
 * Nachts Treppenlicht einschalten
 */
function switchOnDaytime() {
  if (isAstroDay()) {
    log("it's day")
    treppenlicht(true, false)
  } else {
    log("it's night")
    var mode = compareTime('sunset', '23:30', 'between')
    treppenlicht(true, mode)
  }
}

/**
  Licht an im UI
  - Licht unbegrenzt einschalten
*/
on({ id: "javascript.0.aussenlicht_manuell", val: 1 }, function () {
  treppenlicht(false, true)
})

/*
  Licht aus im UI
  - Licht immer ausschalten
*/
on({ id: "javascript.0.aussenlicht_manuell", val: 0 }, function () {
  treppenlicht(false, false)
})

/*
  Licht wird im UI auf "auto" geschaltet
  - Ausschalten, wenn es Tag ist
  - Einschalten, wenn es Nacht aber vor Mitternacht ist
*/
on({ id: "javascript.0.aussenlicht_manuell", val: 2 }, function () {
  log("switched to auto")
  switchOnDaytime()
})


/*
  Die Sonne geht unter
  Licht einschalten, falls es auf "auto" geschaltet ist
*/
schedule({ astro: "sunset", shift: 15 }, function () {
  log("sunset, lights on")
  treppenlicht(true, true)
})

/*
  Es ist 23:30 Uhr
  Licht ausschalten, falls es auf "auto" geschaltet ist
*/
schedule("30 23 * * *", function () {
  log("23:30, lights off")
  treppenlicht(true, false)
})

// /!6!

// !7!
schedule("Zeitausdruck", function () { })
// /!7!

// !8!

log("Hello, World", "info")
schedule("*/10 * * * * *", function () {
  log("und wieder 10 Sekunden vergangen", "info")
})

// /!8!


// !11!
describe("funktionstest", () => {
  it("checks expression", () => {
    expect(2).toBe(3)
  })
})
// /!11!  

// !12!
/**
    Jemand ist in der Erfassungsbereich des Bewegungsmelders getreten
*/
on({id: _motion_detect, val: true}, function(){
  log("motion detector")
  allOn(false)
})

on({id: _motion_detect, val: false}, function(){
 log("motion finished")
 tuerlicht(false)
 setState(_softswitch,"off")
  if(getState("aussenlicht_manuell").val==1){
      treppenlicht(false,false)
  }else{
      switchOnDaytime()
  }
  
})
// /!12!

// !13!
var hue="hue.0.Philips_hue.Wohnzimmer.on";
createState("fernsehlicht_manuell",2)

function licht(val){
    if(getState("fernsehlicht_manuell").val==2){
        if(getState("hue.0.Philips_hue.Wohnzimmer.on").val!=val){
            console.log("schalte Licht: "+val,'debug')
            setState(hue,val)
        }
    }
}

on({id: "javascript.0.fernsehlicht_manuell", val:0},function(){
    setState(hue,true)
})

on({id: "javascript.0.fernsehlicht_manuell", val:1},function(){
    setState(hue,false)
})

schedule({astro: "sunset", shift: -15}, function () {
    log("sunset",'info')
    if(getState("lgtv.0.on"/*TV is ON*/).val){
        licht(true)
    }
});

on({id: 'lgtv.0.on', val: true, change: "ne"}, function(){
    log("tv switched on",'info')
    if(!isAstroDay()){
        licht(true)
    }
})

on({id: 'lgtv.0.on', val: false, change: "ne"}, function(){
    log("tv switched off",'info')
    licht(false)
})

schedule({astro: "sunrise"}, function(){
    log("sunrise")
    licht(false)
})
 
// /!13!

// !14!
var power_pv="fronius.0.powerflow.P_PV" // was wir produzieren
var power_use="fronius.0.powerflow.P_Grid" // was wir vom/zum Netz beziehen/liefern
var mystrom_switch="mystrom.1.switchState" // Der Schalter

createState("loadcar_manual",2)

function toggle(mode){
    // log("auto "+mode+" requested")
    if(getState("javascript.0.loadcar_manual").val==2){
        if(getState(mystrom_switch).val != mode){
            log("toggle "+mode, 'info')
            setState(mystrom_switch,mode)
        }
    }
}

on({id: "javascript.0.loadcar_manual",val: 0},function(){
    log("manual on")
    setState(mystrom_switch,true)
})

on({id: "javascript.0.loadcar_manual",val: 1},function(){
    log("manual off")
    setState(mystrom_switch,false)
})

schedule("*/10 7-19 * * *",function(){
    var net_flow=getState(power_use).val
    // log("available: "+getState(power_pv).val+", net flow: "+net_flow,'info')
    if( net_flow < -1500){
        toggle(true)
    }else if(net_flow > 0){
        toggle(false)
    }
})

schedule("5 21 * * *", function(){
    log("night schedule: on")
    toggle(true)
    
})

schedule("59 6 * * *", function(){
    log("day schedule: off")
    toggle(false)
})
// /!14!